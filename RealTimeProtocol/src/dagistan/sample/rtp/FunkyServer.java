package dagistan.sample.rtp;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.StringTokenizer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.Timer;

/**
 * @author Dagistan
 */
public class FunkyServer extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6680844416986688472L;

	DatagramSocket RTPsocket;
	DatagramPacket senddp;
	InetAddress ClientIPAddr;
	int RTP_dest_port = 0;
	JLabel label;
	int imagenb = 0;
	VideoStream video;
	static int MJPEG_TYPE = 26;
	static int FRAME_PERIOD = 100;
	static int VIDEO_LENGTH = 500;
	Timer timer;
	byte[] buf;
	byte[] oldbuf;
	int oldlength;
	int oldimagenb;
	int round = 0;
	static final int INIT = 0;
	static final int READY = 1;
	static final int PLAYING = 2;
	static final int SETUP = 3;
	static final int PLAY = 4;
	static final int PAUSE = 5;
	static final int TEARDOWN = 6;
	static int state;
	Socket RTSPsocket;
	static BufferedReader RTSPBufferedReader;
	static BufferedWriter RTSPBufferedWriter;
	static String VideoFileName;
	static int RTSP_ID;
	int RTSPSeqNb = 0;
	static final String CRLF = "\r\n";

	static {
		RTSP_ID = 123456;
	}

	public FunkyServer() {
		super("FunkyServer");
		this.timer = new Timer(FRAME_PERIOD, this);
		this.timer.setInitialDelay(0);
		this.timer.setCoalesce(true);
		this.buf = new byte[15000];
		this.oldbuf = new byte[15000];
		this.oldlength = 0;
		addWindowListener(new WindowAdapter() {

			public void windowClosing(WindowEvent paramAnonymousWindowEvent) {
				FunkyServer.this.timer.stop();
				System.exit(0);
			}
		});
		this.label = new JLabel("Send frame #        ", 0);
		this.getContentPane().add((Component) this.label, "Center");
	}

	public void actionPerformed(ActionEvent actionEvent) {
		if (this.imagenb < VIDEO_LENGTH) {
			++this.imagenb;
			try {
				RTPpacket rTPpacket;
				int n;
				int n2 = this.video.getnextframe(this.buf);
				if (this.imagenb == 1) {
					this.oldlength = n2;
					this.oldimagenb = this.imagenb;
					int n3 = 0;
					while (n3 < this.oldlength) {
						this.oldbuf[n3] = this.buf[n3];
						++n3;
					}
				}
				if (this.imagenb % 50 == 0) {
					this.round = 1;
				}
				if (this.round > 0) {
					rTPpacket = new RTPpacket(MJPEG_TYPE, this.oldimagenb, this.oldimagenb * FRAME_PERIOD, this.oldbuf,
							this.oldlength);
					++this.round;
					if (this.round == 10) {
						this.round = 0;
						this.oldlength = n2;
						this.oldimagenb = this.imagenb;
						n = 0;
						while (n < this.oldlength) {
							this.oldbuf[n] = this.buf[n];
							++n;
						}
					}
				} else {
					rTPpacket = new RTPpacket(MJPEG_TYPE, this.imagenb, this.imagenb * FRAME_PERIOD, this.buf, n2);
				}
				n = rTPpacket.getlength();
				byte[] arrby = new byte[n];
				rTPpacket.getpacket(arrby);
				this.senddp = new DatagramPacket(arrby, n, this.ClientIPAddr, this.RTP_dest_port);
				this.RTPsocket.send(this.senddp);
				System.out.println("Send frame #" + this.imagenb);
				rTPpacket.printheader();
				this.label.setText("Send frame #" + this.imagenb);
			} catch (Exception var2_3) {
				System.out.println("Exception caught : " + var2_3);
				System.exit(0);
			}
		} else {
			this.timer.stop();
		}
	}

	public static void main(String[] arrstring) throws Exception {
		int n;
		FunkyServer funkyServer = new FunkyServer();
		funkyServer.pack();
		funkyServer.setVisible(true);
		int n2 = 2147;
		ServerSocket serverSocket = new ServerSocket(n2);
		funkyServer.RTSPsocket = serverSocket.accept();
		serverSocket.close();
		funkyServer.ClientIPAddr = funkyServer.RTSPsocket.getInetAddress();
		state = 0;
		RTSPBufferedReader = new BufferedReader(new InputStreamReader(funkyServer.RTSPsocket.getInputStream()));
		RTSPBufferedWriter = new BufferedWriter(new OutputStreamWriter(funkyServer.RTSPsocket.getOutputStream()));
		boolean bl = false;
		while (!bl) {
			n = funkyServer.parse_RTSP_request();
			if (n != 3)
				continue;
			bl = true;
			state = 1;
			System.out.println("New RTSP state: READY");
			funkyServer.send_RTSP_response();
			funkyServer.video = new VideoStream(VideoFileName);
			funkyServer.RTPsocket = new DatagramSocket();
		}
		do {
			if ((n = funkyServer.parse_RTSP_request()) == 4 && state == 1) {
				funkyServer.send_RTSP_response();
				funkyServer.timer.start();
				state = 2;
				System.out.println("New RTSP state: PLAYING");
				continue;
			}
			if (n == 5 && state == 2) {
				funkyServer.send_RTSP_response();
				funkyServer.timer.stop();
				state = 1;
				System.out.println("New RTSP state: READY");
				continue;
			}
			if (n != 6)
				continue;
			funkyServer.send_RTSP_response();
			funkyServer.timer.stop();
			System.exit(0);
		} while (true);
	}

	private int parse_RTSP_request() {
		int n = -1;
		try {
			String string = RTSPBufferedReader.readLine();
			System.out.println(string);
			StringTokenizer stringTokenizer = new StringTokenizer(string);
			String string2 = stringTokenizer.nextToken();
			if (new String(string2).compareTo("SETUP") == 0) {
				n = 3;
			} else if (new String(string2).compareTo("PLAY") == 0) {
				n = 4;
			} else if (new String(string2).compareTo("PAUSE") == 0) {
				n = 5;
			} else if (new String(string2).compareTo("TEARDOWN") == 0) {
				n = 6;
			}
			if (n == 3) {
				VideoFileName = stringTokenizer.nextToken();
			}
			String string3 = RTSPBufferedReader.readLine();
			System.out.println(string3);
			stringTokenizer = new StringTokenizer(string3);
			stringTokenizer.nextToken();
			this.RTSPSeqNb = Integer.parseInt(stringTokenizer.nextToken());
			String string4 = RTSPBufferedReader.readLine();
			System.out.println(string4);
			if (n == 3) {
				stringTokenizer = new StringTokenizer(string4);
				int n2 = 0;
				while (n2 < 3) {
					stringTokenizer.nextToken();
					++n2;
				}
				this.RTP_dest_port = Integer.parseInt(stringTokenizer.nextToken());
			}
		} catch (Exception var2_3) {
			System.out.println("Exception caught : " + var2_3);
			System.exit(0);
		}
		return n;
	}

	private void send_RTSP_response() {
		try {
			RTSPBufferedWriter.write("RTSP/1.0 200 OK\r\n");
			RTSPBufferedWriter.write("CSeq: " + this.RTSPSeqNb + "\r\n");
			RTSPBufferedWriter.write("Session: " + RTSP_ID + "\r\n");
			RTSPBufferedWriter.flush();
		} catch (Exception var1_1) {
			System.out.println("Exception caught : " + var1_1);
			System.exit(0);
		}
	}
}