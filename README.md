# README #

Run as Java Application. Real Time Protocol "Server" or "FunkyServer" will be run first. Then run Client and see what happens. FunkyServer has some bugs. check console to see the problems (or use wireshark. ;))

### Used Technologies for all projects ###

* Java SE,
* Java EE,
* 3rd party Java frameworks / libraries.

### Requirements ###

* Eclipse IDE
* JDK 1.8
* Deployment and Run instructions are included in related project class files. Read carefully.

I hope you enjoy :)